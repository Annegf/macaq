During my PhD, I worked on Question-Answering systems and collected a corpus of French native speaker answers to questions. The main characteristics of this corpus are:
  * Questions used have a controlled linguistic form
  * It is both oral and written
  * Answers are made for question-answering system use
  * It has been annotated on different level
  * It is unique so far! (april 2011)
  * It is freely available upon request

For more detail, see the LREC 2010 paper.
The corpus is available upon mail request (only the transcribed version of the oral answers is available).
If you use it please, let me know how and don't forget to cite the LREC 2010 paper :
```bibtex

 @INPROCEEDINGS{Garcia-Fernandez_LREC2010,
   author = {Anne Garcia-Fernandez, Sophie Rosset, Anne Vilnat},
   title = {MACAQ : A Multi Annotated Corpus to study how we adapt Answers to various Questions},
   booktitle = {Proceedings of LREC},
   year = {2010},
 }
```

Associated Publications:
  * Generation of speech and written answers in natural language for question-answering systems in open domain
  * Speech and written : which annotation for doing multimodal comparison?
AnneGF's PhD defense, 10.12.2010
  * Euh as Cue for Speaker Confidence and Word Searching in Human Spoken Answers in French
  * How do we formulate answers?
  * MACAQ : A Multi Annotated Corpus to Study how we Adapt Answers to Various Questions
  * Natural answers collect and analysis for question answering systems
  * Interrogation morphosyntax for the RITEL question-answering system